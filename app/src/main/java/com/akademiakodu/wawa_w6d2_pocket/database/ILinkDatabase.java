package com.akademiakodu.wawa_w6d2_pocket.database;

import com.akademiakodu.wawa_w6d2_pocket.model.Link;

import java.util.List;

public interface ILinkDatabase {
    List<Link> getLinks();

    void create(Link link);

    Link getLink(int id);

    void update(Link link);

    void delete(Link link);
}
